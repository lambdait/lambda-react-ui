import db from './article.json';

const article =  () => {
    return fetch('/article', {
        gzip: true
    }).then(data => data.json()).then(data => data).catch(e => db)
};

export default article;
