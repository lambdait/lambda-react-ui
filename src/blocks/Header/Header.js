import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Menu from 'e:Menu'
import Logo from 'e:Logo'
import Socials from 'e:Socials'
import Sidebar from 'b:Sidebar'

export default decl({
    block: 'header',
    tag: 'nav',
    willInit() {
        this.state = {
            open_sidebar: false
        };
        this._toogleSidebar = this._toogleSidebar.bind(this);
    },
    _toogleSidebar() {
        this.setState({open_sidebar: !this.state.open_sidebar});
    },
    _watchScroll(e) {
        if (window.scrollY > 1) {
            this.setState({open_sidebar: false})
        }
    },
    willMount() {
        document.addEventListener('scroll', e => this._watchScroll(e));
    },
    content({menu, social}) {
        return (
            <Fragment>
                <Menu upateSidebar={this._toogleSidebar} toogle={this.state.open_sidebar}/>
                <Logo/>
                <Socials/>
                <Sidebar list={menu} toogle={this.state.open_sidebar}/>
            </Fragment>
        )
    }
})
