import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Item from 'e:Item'

export default decl({
    block: 'header',
    elem: 'socials',
    tag: 'ul',
    content({date_create}) {
        return (
            <Fragment>
                <Item/>
            </Fragment>
        )
    }
})
