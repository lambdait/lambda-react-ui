import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Icon from 'b:Icon';
import {ICONS} from '../../Icon/icon_constants';

export default decl({
    block: 'header',
    elem: 'menu',
    willInit() {
        this._onMouseClick = this._onMouseClick.bind(this);
    },
    attrs() {
        return {
            onClick: this._onMouseClick
        }
    },
    _onMouseClick(e) {
        this.props.upateSidebar();
    },
    content({toogle}) {
        return (
            <Fragment>
                <Icon size={30} icon={ICONS.MENU} color={"#d8d8d8"}/>
            </Fragment>
        )
    }
})
