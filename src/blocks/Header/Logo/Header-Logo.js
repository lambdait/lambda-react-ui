import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Icon from 'b:Icon';
import {ICONS} from '../../Icon/icon_constants';

export default decl({
    block: 'header',
    elem: 'logo',
    content() {
        return(
            <Fragment>
                <Icon width={100} height={22} icon={ICONS.LAMBDA} color={"#4D4D4D"}/>
            </Fragment>
        )
    }
})
