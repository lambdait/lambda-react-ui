import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';

export default decl({
    block: 'header',
    elem: 'link',
    tag: 'a',
    attrs({to, disabled}) {
        let res = {
            onClick: this._onMouseClick,
            href: to,
            rel: 'noreferrer'
        };
        if (!disabled) {
            res = {
                ...res,
                onFocus: this._onFocus,
                onBlur: this._onBlur,
                onMouseEnter: this._onMouseEnter,
                onMouseLeave: this._onMouseLeave,
                onMouseDown: this._onMouseDown,
                onMouseUp: this._onMouseUp
            };

            if (this.state.focused)
                res = {
                    ...res,
                    onKeyDown: this._onKeyDown,
                    onKeyUp: this._onKeyUp
                };

        }
        return res;
    },
    willInit() {
        this.state = {
            hovered: false,
            pressed: false
        };

        this._isMousePressed = false;

        this._onMouseEnter = this._onMouseEnter.bind(this);
        this._onMouseLeave = this._onMouseLeave.bind(this);
    },
    mods({disabled, checked}) {
        const {hovered} = this.state;
        return {
            ...this.__base(...arguments),
            hovered,
        };
    },


    _onMouseEnter() {
        this.setState({hovered: true});
    },
    _onMouseLeave() {
        this._isMousePressed = false;
        this.setState({hovered: false, pressed: false});
    },
    content({to, children}) {
        return (
            <Fragment>
                {children}
            </Fragment>
        )
    }
})
