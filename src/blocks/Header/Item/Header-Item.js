import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Link from 'e:Link';
import Icon from 'b:Icon';
import {ICONS} from '../../Icon/icon_constants';

export default decl({
    block: 'header',
    elem: 'item',
    tag: 'li',
    content() {
        return (
            <Fragment>
                <Link to="#">
                    <Icon size={30} icon={ICONS.TWITTER} color={"#ccc"} hoverColor={"#000"}/>
                </Link>
            </Fragment>
        )
    }
})
