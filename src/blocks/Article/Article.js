import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import ArticleHeader from 'b:ArticleHeader m:type_centred m:type_withoutImage'
import PropTypes from 'prop-types';
import ArticleContent from 'b:ArticleContent'
import ArticleFooter from 'b:ArticleFooter'
export default decl({
    block: 'article',
    content({
                title, image, categoryColor, categoryName, date, views, author, countComment,
                content, author_name, author_image, author_position, comments, likes, tags,shares
            }) {
        console.log(author_position);
        return (
            <Fragment>
                <ArticleHeader title={title} image={image} categoryColor={categoryColor}
                               categoryName={categoryName} date={date}
                               views={views} author={author} countComment={countComment}
                               type={'centred'}
                />
                <ArticleContent content={content}/>
                <ArticleFooter author_name={author_name} author_image={author_image} auhtor_position={author_position}
                                views={views} comments={comments} likes={likes} tags={tags} shares={shares} />
            </Fragment>
        )
    }
}, {
    propTypes: {
        /** Название статьи */
        title: PropTypes.string,
        /** Ссылка на изображение */
        image: PropTypes.string,
        /** Название категории */
        categoryName: PropTypes.string,
        /** Цвет категории */
        categoryColor: PropTypes.string,
        /** Дата создания  */
        date: PropTypes.string,
        /** Количество просмотров */
        views: PropTypes.number,
        /** Автор статьи */
        author: PropTypes.string,
        /** Количество комментариев */
        countComment: PropTypes.number,

    },
    defaultProps: {
        title: "Название статьи",
        image: "Отсутствие изображения",
        categoryName: "Категория",
        categoryColor: "pink",
        date: "14 Апр. 15:33",
    }
})
