import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Content from 'e:Content'

export default decl({
    block: 'navigation',
    tag: 'nav',
    mods({type}) {
        return {type};
    },
    content({title, list, callback}) {
        return (
            <Fragment>
                <Content title={title} list={list} callback={callback}/>
            </Fragment>
        )
    }
})

