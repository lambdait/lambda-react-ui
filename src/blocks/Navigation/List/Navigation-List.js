import React from 'react';
import {decl} from 'bem-react-core';
import Item from 'e:Item'

export default decl({
    block: 'navigation',
    elem: 'list',
    tag: 'ul',
    willInit() {
        this.state = {
            activeCategory: 'all'
        };
        this._setActiveCategory = this._setActiveCategory.bind(this);
    },
    _setActiveCategory(slug) {
        this.setState({activeCategory: slug});
        this.props.callback(slug)
    },
    content({list}) {
        return list && list.map((el, index) =>
            <Item text={el.title} slug={el.slug}
                  activeSlug={this.state.activeCategory}
                  setCategory={this._setActiveCategory}
                  key={index}/>
        )
    }
})
