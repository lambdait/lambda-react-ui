import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';

export default decl({
    block: 'navigation',
    elem: 'item',
    tag: 'li',
    attrs({active}) {
        let res = {
            onMouseEnter: this._onMouseEnter,
            onMouseLeave: this._onMouseLeave,
            onClick: this._onMouseClick
        };
        return res
    },
    willInit({slug, activeSlug}) {
        this.state = {
            hovered: false,
            active: slug === activeSlug
        };
        this._onMouseClick = this._onMouseClick.bind(this);
        this._onMouseEnter = this._onMouseEnter.bind(this);
        this._onMouseLeave = this._onMouseLeave.bind(this);
    },
    willReceiveProps({slug, activeSlug}) {
        this.setState({active:slug === activeSlug})
    },
    mods() {
        const {hovered, active} = this.state;
        return {
            ...this.__base(...arguments),
            hovered,
            active
        }
    },
    _onMouseEnter() {
        this.setState({hovered: true});
    },
    _onMouseLeave() {
        this.setState({hovered: false});
    },

    _onMouseClick() {
        if (this.props.slug !== this.props.activeSlug) {
            this.props.setCategory(this.props.slug);
        }
    },
    content({text}) {
        return (
            <Fragment>
                {text}
            </Fragment>
        )

    }
})
