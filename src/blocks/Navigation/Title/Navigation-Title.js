import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';

export default decl({
    block: 'navigation',
    elem: 'title',
    content({children}) {
        return (
            <Fragment>
                <span>{children}</span>
            </Fragment>
        )

    }
})
