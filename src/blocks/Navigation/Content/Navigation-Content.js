import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Title from 'e:Title'
import List from 'e:List'

export default decl({
    block: 'navigation',
    elem: 'content',
    content({title, list, callback}) {
         return (
            <Fragment>
                <Title>{title}</Title>
                <List list={list} callback={callback}/>
            </Fragment>
        )

    }
})
