import { decl } from 'bem-react-core';

export default decl({
    block : 'button',
    elem : 'text',
    tag : 'span'
});
