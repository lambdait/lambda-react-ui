import React from 'react';

import {decl} from 'bem-react-core';
import Card
    from 'b:Card m:image_fill m:size_xxl m:size_xl_rectangle m:size_m m:size_l m:size_xs m:size_xss_wide m:size_xs_wide m:size_square_xl m:image_flat'

export default decl({
    block: 'feed',
    elem: 'list',
    size: ['xss_wide', 'l', 'xs', 'm', 'xl_rectangle'],
    mods({column}) {
        return {column};
    },
    content({feed}) {

        if (!feed) {
            return null;
        }
        // TODO: Карточки в списке остаются
        // card_size_xxl - остается
        // card_size_m card_image_flat - остается
        // card_size_l - остается
        // card_size_xs card_image_fill - остается
        // card_size_square_xl card_image_fill - остается
        // card_size_m card_image_flat - остается
        // card_size_l card_image_flat - остается
        return feed.map((el) =>
            el.title ?
                <Card
                    tag_name={el.categoryName.name}
                    tag_color={el.categoryName.color}
                    key={el.id}
                    thumbnail={el.thumbnail}
                    title={el.title}
                    author={el.author.name}
                    descriptions={el.title}
                    views={el.size}
                    comment={el.size}
                    size={el.size}
                    date_create={el.dateCreate}
                    image={el.image_size}
                /> : null)
    },
})
