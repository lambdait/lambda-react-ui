import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import article from '../../db/article.js';
import './Feed.sass'
import Header from 'b:Header'
import Article from 'b:Article'
import Breadcrumbs from 'b:Breadcrumbs'
import client from '../../db/client.js';
import Navigation from 'b:Navigation m:type_fluid'
import FeedList from 'e:List m:column_4 m:column_3';

export default decl({
    block: 'feed',
    willInit() {
        this.state = {
            feed: [],
            isLoading: false
        };
        this._callBackNavigation = this._callBackNavigation.bind(this);
    },
    _loadFeed() {
        this.setState({isLoading: true}, () => {
            client().then(data => {
                this.setState({
                    feed: data,
                    isLoading: false
                })
            });
        });
    },
    willMount() {
        this._loadFeed();
    },
    _callBackNavigation(slug) {
        console.log(slug);
    },

    content() {
        let head_menu = [
            {
                text: 'Главная',
                to: '/'
            }, {
                text: 'Публикации',
                to: 'ref'
            },
        ];
        let nav_menu = [
            {
                "title": "Всё",
                "slug": "all"
            },
            {
                "title": "Спикер",
                "slug": "spices"
            },
            {
                "title": "ML",
                "slug": "ml"
            }
        ];
        return (
            <Fragment>
                <Header menu={head_menu}/>
                <Navigation title="Публикации" type="fluid" callback={this._callBackNavigation} list={nav_menu}/>
                <FeedList feed={this.state.feed}/>
            </Fragment>
        )
    }
})
