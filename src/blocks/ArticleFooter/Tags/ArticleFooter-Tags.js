import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Button from 'b:Button m:type'

export default decl({
    block: 'articlefooter',
    elem: 'tags',

    content({tags}) {
        return (
            <Fragment>
                {
                    tags.map((tag, index)=>{
                        return <Button key={index} text={tag} type='label' />
                    })
                }
            </Fragment>
        )
    }
})
