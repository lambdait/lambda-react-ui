import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Title from 'e:Title'
import Share from 'e:Share'

export default decl({
    block: 'articlefooter',
    elem: 'shares',
    content({shares}) {
        return (
            <Fragment>
            <Title />
                {
                    shares.map((share, index) =>{
                        return <Share key={index} name={share.name} count={share.count} />
                    })
                }
            </Fragment>
        )
    }
})
