import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';

export default decl({
    block: 'shares',
    elem: 'title',

    tag() {
        return 'span';
    },

    content({shares}) {
        return (
            <Fragment>
                Поделиться:
            </Fragment>
        )
    }
})