import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Author from 'e:Author'
import Tools from 'e:Tools'
import Tags from 'e:Tags'
import Shares from 'e:Shares'
import Buttton from 'b:Button m:icon_align m:type'
import Icon from 'b:Icon';
import {ICONS} from '../Icon/icon_constants';

export default decl({
    block: 'articlefooter',
    toTop() {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    },
    tag: 'div',
    content({author_name, author_image, auhtor_position, views, comments, likes, tags, shares}) {
        let icon = <Icon size={30} icon={ICONS.ARROW_UP} color={"#d8d8d8"}/>
        const divStyle = {
            justifyContent: 'center',
        };

        return (
            <Fragment>
                <div className='articlefooter-top'>
                    <div className='articlefooter-item'>
                        <Author name={author_name} image={author_image} position={auhtor_position}/>
                    </div>
                    <div className='articlefooter-item' style={divStyle}>
                        <Tools views={views} comments={comments} likes={likes}/>
                    </div>
                    <div className='articlefooter-item'>
                        <Buttton icon={icon} text='Наверх' onClick={this.toTop} type='ghost' icon_align='right'/>
                    </div>
                </div>
                <div className='articlefooter-bottom'>
                    <Tags tags={tags}/>
                    <Shares shares={shares}/>
                </div>
            </Fragment>
        )
    }
})
