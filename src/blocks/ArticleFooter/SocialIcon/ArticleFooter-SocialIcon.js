import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';

export default decl({
    block: 'articlefooter',
    elem: 'socialicon',
    tag : 'span',

    mods({name}) {
     return {
      ...this.__base(...arguments),
      name
     };
    },

    content() {
        return (
            <Fragment></Fragment>
        )
    }
})