import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Icon from 'b:Icon';
import {ICONS} from '../../Icon/icon_constants';


export default decl({
    block: 'articlefooter',
    elem: 'tools',

    content({views, likes, comments}) {

        return (
            <Fragment>
                <div className='articlefooter-tools-views'>
                    <Icon size={30} icon={ICONS.EYE} color={"#d8d8d8"}/> {views}
                </div>
                <div className='articlefooter-tools-likes' >
                    <Icon size={30} icon={ICONS.FAVORITE} color={"#d8d8d8"}/> {likes}
                </div>
                <div className='articlefooter-tools-comment'>
                    <Icon size={30} icon={ICONS.FORUM} color={"#d8d8d8"}/> {comments}
                </div>
            </Fragment>
        )
    }
})
