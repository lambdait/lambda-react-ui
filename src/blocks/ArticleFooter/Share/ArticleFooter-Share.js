import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import ShareCount from 'e:ShareCount';
import SocialIcon from 'e:SocialIcon m:name';

export default decl({
    block: 'articlefooter',
    elem: 'share',

    content({name, count}) {
        return (
            <Fragment>
                <SocialIcon name={name} />
                <ShareCount count={count} />
            </Fragment>
        )
    }
})