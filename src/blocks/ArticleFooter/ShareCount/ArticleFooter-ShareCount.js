import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';

export default decl({
    block: 'articlefooter',
    elem: 'sharecount',

    content({count}) {
        return (
            <Fragment>{count}</Fragment>
        )
    }
})