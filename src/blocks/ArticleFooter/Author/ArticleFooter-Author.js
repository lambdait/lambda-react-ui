import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';

export default decl({
    block: 'articlefooter',
    elem: 'author',

    content({image, name, position}) {
        return (
            <Fragment>
                <img  src={image} alt={name}/>
                <div className='articlefooter-author-info'>
                    <div className='articlefooter-author-name'>{name}</div>
                    <div className='articlefooter-author-position'>{position}</div>
                </div>
            </Fragment>
        )
    }
})
