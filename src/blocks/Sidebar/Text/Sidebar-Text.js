import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';

export default decl({
    block: 'sidebar',
    elem: 'text',
    tag: 'li',
    content({text, to}) {
        return (
            <Fragment>
                <a href={to} className="sidebar-link">{text}</a>
            </Fragment>
        )
    }
})
