import React from 'react';
import {decl} from 'bem-react-core';
import Text from 'e:Text'

export default decl({
    block: 'sidebar',
    elem: 'list',
    tag: 'ul',
    content({list}) {
        return list.map(el => (
            <Text key={el.text} text={el.text} to={el.to}/>
        ))

    }
})
