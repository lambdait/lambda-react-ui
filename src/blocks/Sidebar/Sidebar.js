import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import List from 'e:List'
import PropTypes from 'prop-types';

export default decl({
    block: 'sidebar',
    willInit({toogle}) {
        this.state = {
            open: toogle
        };

    },
    mods() {
        const {open} = this.state;
        return {
            ...this.__base(...arguments),
            open
        }
    },
    /**
         Получение обновленных свойств
     */
    willReceiveProps(nextProps) {
        this.setState({open: nextProps.toogle});
    },

    content({list}) {
        return (
            <Fragment>
                <List list={list}/>
            </Fragment>
        )

    }
}, {
    propTypes: {
        /** Список вывода элементов  */
        list: PropTypes.array,
        /** Отображение сайд бара  */
        toogle: PropTypes.bool
    }, 
    
})
