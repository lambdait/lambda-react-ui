import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';

export default decl({
    block: 'articleheader',
    elem: 'tools',
    content({children}) {
        return (
            <Fragment>
                {children}
            </Fragment>
        )
    }
})
