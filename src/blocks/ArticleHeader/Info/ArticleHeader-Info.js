import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Item from 'e:Item'
import {ICONS} from "../../Icon/icon_constants";

export default decl({
    block: 'articleheader',
    elem: 'info',
    content({views, author, countComment}) {
        return (
            <Fragment>
                <Item icon={ICONS.EYE} value={views}/>
                <Item icon={ICONS.FORUM} value={countComment}/>
                <Item icon={ICONS.PROFILE} value={author}/>
            </Fragment>
        )
    }
})
