import React from 'react';
import {declMod} from 'bem-react-core';
import Button from 'b:Button m:rounded_circle m:color_white';
import Content from 'e:Content'
import Tools from 'e:Tools'
import Info from 'e:Info'
import Main from 'e:Main'
import Icon from 'b:Icon';
import {ICONS} from '../../Icon/icon_constants';

export default declMod({type: 'centred'}, {
    block: 'articleheader',
    content({title, categoryName, categoryColor, date, views, author, countComment}) {
        return (
            <Content>
                <Tools><Button rounded={"circle"}>
                        <Icon size={30} icon={ICONS.SHARE} color={"#fff"}/>
                    </Button>
                </Tools>
                <Main title={title} categoryName={categoryName}
                      categoryColor={categoryColor} date={date}/>
                <Tools>
                    <Button rounded={"circle"}>
                        <Icon size={30} icon={ICONS.FAVORITE_BORDER} color={"#fff"}/>
                    </Button>
                </Tools>
                <Info views={views} author={author} countComment={countComment}/>
            </Content>
        );
    }
});
