import React from 'react';
import {declMod} from 'bem-react-core';
import Button from 'b:Button m:rounded_circle m:color_white';
import Content from 'e:Content'
import Tools from 'e:Tools'
import Info from 'e:Info'
import Main from 'e:Main'
import Icon from 'b:Icon';
import {ICONS} from '../../Icon/icon_constants';

export default declMod({type: 'withoutImage'}, {
    block: 'articleheader',
    style() {
        let res = {
            backgroundColor: 'white',
        };
        return res;
    },
    content({title, categoryName, categoryColor, date, views, author, countComment}) {
        return (
            <Content>
                <Tools>
                    <Button rounded={"circle"} color={"white"}>
                        <Icon size={30} icon={ICONS.SHARE} color={"#d8d8d8"}/>
                    </Button>
                </Tools>
                <Main title={title} categoryName={categoryName}
                      categoryColor={categoryColor} date={date}/>
                <Tools>
                    <Button rounded={"circle"} color={"white"}>
                        <Icon size={30} icon={ICONS.FAVORITE_BORDER} color={"#d8d8d8"}/>
                    </Button>
                </Tools>
                <Info views={views} author={author} countComment={countComment}/>
            </Content>
        );
    }
});
