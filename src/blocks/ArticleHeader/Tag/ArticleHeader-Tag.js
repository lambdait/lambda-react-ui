import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';


export default decl({
    block: 'articleheader',
    elem: 'tag',
    tag: 'span',
    style({color}) {
        return {
            background: color,
        }
    },
    content({tag}) {
        return (
            <Fragment>
                {tag}
            </Fragment>
        )
    }
})
