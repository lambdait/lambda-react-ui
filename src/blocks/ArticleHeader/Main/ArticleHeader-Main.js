import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Title from 'e:Title'
import Tag from 'e:Tag'
import Date from 'e:Date'

export default decl({
    block: 'articleheader',
    elem: 'main',
    content({title, image, categoryName, categoryColor, date}) {
        return (
            <Fragment>
                <Tag tag={categoryName} color={categoryColor}/>
                <Date>{date}</Date>
                <Title>{title}</Title>
                <span className="articleheader-line" style={{borderColor: `${categoryColor}`}}></span>
            </Fragment>
        )
    }
})
