import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';

export default decl({
    block: 'articleheader',
    elem: 'title',
    tag: 'h1',
    content({children}) {
        return (
            <Fragment>
                {children}
            </Fragment>
        )
    }
})
