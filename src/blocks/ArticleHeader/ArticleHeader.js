import React from 'react';
import {decl} from 'bem-react-core';
import PropTypes from 'prop-types';
import Content from 'e:Content'
import Tools from 'e:Tools'
import Info from 'e:Info'
import Main from 'e:Main'
import Button from 'b:Button m:rounded_circle m:color_white';
import Icon from 'b:Icon';
import {ICONS} from '../Icon/icon_constants';

export default decl({
    block: 'articleheader',
    style({title, image}) {
        let res = {
            backgroundImage: `url(${image})`,
            backgroundPosition: 'center',
        };
        return res;
    },
    content({title, categoryName, categoryColor, date, views, author, countComment}) {
        return (
            <Content>
                <Main title={title} categoryName={categoryName}
                      categoryColor={categoryColor} date={date}/>
                <Tools>
                <Button rounded={"circle"}>
                        <Icon size={30} icon={ICONS.SHARE} color={"#fff"}/>
                    </Button>
                    <Button rounded={"circle"}>
                        <Icon size={30} icon={ICONS.FAVORITE_BORDER} color={"#fff"}/>
                    </Button>
                </Tools>
                <Info views={views} author={author} countComment={countComment}/>
            </Content>
        )
    }
}, {
    propTypes: {
        /** Название статьи */
        title: PropTypes.string,
        /** Ссылка на изображение */
        image: PropTypes.string,
        /** Название категории */
        categoryName: PropTypes.string,
        /** Цвет категории */
        categoryColor: PropTypes.string,
        /** Дата создания  */
        date: PropTypes.string,
        /** Количество просмотров */
        views: PropTypes.number,
        /** Автор статьи */
        author: PropTypes.string,
        /** Количество комментариев */
        countComment: PropTypes.number,
        /** Тип */
        type: PropTypes.oneOf(['centred', 'withoutImage', 'normal']).isRequired
    }
})
