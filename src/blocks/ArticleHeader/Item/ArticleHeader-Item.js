import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Icon from 'b:Icon';


export default decl({
    block: 'articleheader',
    elem: 'item',
    tag: 'span',
    content({icon, value}) {
        return (
            <Fragment>
                <Icon size={30} icon={icon} color={"#ccc"} />
                <span className={"articleheader-value"}>{value}</span>
            </Fragment>
        )
    }
})
