import React, {Fragment} from 'react';
import {declMod} from 'bem-react-core';

export default declMod({active: true}, {
    block: 'breadcrumbs',
    elem: 'item',
    tag: 'li',
    content({text}) {
        return (
            <Fragment>
                <span>{text}</span>
            </Fragment>
        );
    }
});
