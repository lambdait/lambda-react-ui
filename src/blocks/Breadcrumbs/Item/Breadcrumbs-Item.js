import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Icon from 'b:Icon';
import {ICONS} from '../../../blocks/Icon/icon_constants';

export default decl({
    block: 'breadcrumbs',
    elem: 'item',
    tag: 'li',
    mods({active}) {
        const {hovered} = this.state;
        return {
            ...this.__base(...arguments),
            active,
            hovered
        }
    },
    attrs() {
        let res = {
            onMouseEnter: this._onMouseEnter,
            onMouseLeave: this._onMouseLeave,
        };
        return res
    },
    willInit() {
        this.state = {
            hovered: false
        };
        this._onMouseEnter = this._onMouseEnter.bind(this);
        this._onMouseLeave = this._onMouseLeave.bind(this);
    },
    willReceiveProps({slug, activeSlug}) {
        this.setState({active: slug === activeSlug})
    },

    _onMouseEnter() {
        this.setState({hovered: true});
    },
    _onMouseLeave() {
        this.setState({hovered: false});
    },

    content({text, to}) {
        return (
            <Fragment>
                <a href={to}>{text}</a><Icon size={30} icon={ICONS.CHEVRON_RIGHT} color={"#ccc"}/>
            </Fragment>

        )

    }
})
