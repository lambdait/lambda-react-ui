import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import List from 'e:List'

export default decl({
    block: 'breadcrumbs',
    tag: 'nav',
    content({list, callback}) {
        return (
            <Fragment>
                <List list={list} callback={callback}/>
            </Fragment>
        )
    }
})
