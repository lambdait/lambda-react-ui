import React from 'react';
import {decl} from 'bem-react-core';
import Item from 'e:Item m:active'

export default decl({
    block: 'breadcrumbs',
    elem: 'list',
    tag: 'ol',
    content({list}) {
        return list.map((el, index) => (
            <Item key={el.title} text={el.title} active={el.active} to={el.to}/>
        ))

    }
})
