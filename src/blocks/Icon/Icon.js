/*import React from 'react';
import PropTypes from 'prop-types';

const Icon = props => {

    const styles = {
        svg: {
            display: 'inline-block',
            verticalAlign: 'middle',
        },
        path: {
            fill: props.color,
        },
    };


    return (
        <svg
            style={styles.svg}
            width={`${props.size}px`}
            height={`${props.size}px`}
            viewBox="7 7 16 16">
            <path
                style={styles.path}
                d={props.icon}
            ></path>
        </svg>
    );
};

Icon.propTypes = {
    icon: PropTypes.string.isRequired,
    size: PropTypes.number,
    color: PropTypes.string,
    hoverColor: PropTypes.string
};

Icon.defaultProps = {
    size: 16,
};

export default Icon;
*/

import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Path from 'e:Path';

export default decl({
    block: 'icon',
    tag: 'svg',
    attrs({size, height, width}) {
        if (height && width) {
            let res = {
                width: `${width}px`,
                height: `${height}px`,
                viewBox: `0 0 ${width} ${height}`
            };
            return res
        }
        else {

            let res = {
                width: `${size}px`,
                height: `${size}px`,
                viewBox: "0 0 30 30"
            };
            return res
        }
    },

    content({icon, color, hoverColor}) {
        return (
            <Fragment>
                <Path path={icon} color={color} hoverColor={hoverColor}/>
            </Fragment>
        )
    }
})
