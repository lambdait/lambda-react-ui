import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';

export default decl({
    block: 'icon',
    elem: 'path',
    tag: 'path',
    attrs({path, color}) {
        let res = {
            fill: color,
            d: path,
        };
        return res
    }
})
