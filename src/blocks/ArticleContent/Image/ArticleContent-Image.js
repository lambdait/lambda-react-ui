import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Button from 'b:Button m:rounded_circle m:color_white';
import {Intense} from './Intense'
import ReactDOM from 'react-dom'
import Icon from 'b:Icon';
import {ICONS} from '../../Icon/icon_constants';

export default decl({
    block: 'articlecontent',
    elem: 'image',
    mods({size}) {
        return {size};
    },
    didMount() {
        //* Просмотр в полный экран *//
        Intense(ReactDOM.findDOMNode(this).children[0]);
    },
    content({image, caption, thumbnail}) {
        return (
            <Fragment>
                <div data-image={image} data-title={caption} className="articlecontent-image-view">
                    <Button rounded={"circle"}>
                        <Icon size={30} icon={ICONS.SEARCH} color={"#fff"}/>
                    </Button>
                </div>
                <img src={thumbnail} alt={caption}/>
                <span className="articlecontent-image-caption">{caption}</span>
            </Fragment>
        )
    }
})
