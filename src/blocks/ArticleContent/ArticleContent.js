import React from 'react';
import {decl} from 'bem-react-core';
import Content from 'e:Content m:block_carousel m:block_code m:block_text m:block_gallery m:block_quote ';

export default decl({
    block: 'articlecontent',
    content({content}) {
        return (content.map((el, index) =>
                <Content block={el.block} images={el.images}
                         content={el} key={el.block}></Content>))
    }
})
