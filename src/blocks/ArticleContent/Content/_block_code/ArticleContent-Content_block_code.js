import React from 'react';
import {declMod} from 'bem-react-core';
import Highlight from 'react-highlight'
import './dracula.css'

export default declMod({block: 'code'}, {
    block: 'articlecontent',
    elem: 'content',
    content({content}) {
        return (
            <Highlight className={content.lang}>
                {content.content}
            </Highlight>
        );
    }
});
