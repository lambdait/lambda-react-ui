import React, {Fragment} from 'react';
import {declMod} from 'bem-react-core';
import Image from 'e:Image m:size_wide'
import Button from 'b:Button m:rounded_circle m:color_white';
import {Carousel} from './Carousel'
import Icon from 'b:Icon';
import {ICONS} from '../../../Icon/icon_constants';

export default declMod({block: 'carousel'}, {
    block: 'articlecontent',
    elem: 'content',
    didMount() {
        new Carousel({
            "wrap": ".articlecontent-content-wrap",
            "prev": ".articlecontent-content-left",
            "next": ".articlecontent-content-right",
            "touch": true,
            "autoplay": false,
            "autoplayDelay": 3000
        });
    },
    content({images}) {
        return (
            <Fragment>
                <div className="articlecontent-content-left">
                    <Button rounded={"circle"} color={"white"}>
                        <Icon size={30} icon={ICONS.ARROW_LEFT} color={"#d8d8d8"}/>
                    </Button>
                </div>
                <div className="articlecontent-content-area">
                    <div className="articlecontent-content-wrap">
                        {images && images.map((el, index) =>
                            <div key={index} className="articlecontent-content-item">
                                <Image size="wide" image={el.image}
                                       caption={el.caption} thumbnail={el.thumbnail}/>
                            </div>
                        )}
                    </div>
                </div>
                <div className="articlecontent-content-right">
                    <Button rounded={"circle"} color={"white"}>
                        <Icon size={30} icon={ICONS.ARROW_RIGHT} color={"#d8d8d8"}/>
                    </Button>
                </div>
            </Fragment>
        );
    }
});
