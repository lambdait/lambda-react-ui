import React, {Fragment} from 'react';
import {declMod} from 'bem-react-core';

export default declMod({block: 'text'}, {
    block: 'articlecontent',
    elem: 'content',
    content({content}) {
        return (
            <Fragment>
                <div dangerouslySetInnerHTML={{__html: content.content}}>
                </div>
            </Fragment>
        );
    }
});
