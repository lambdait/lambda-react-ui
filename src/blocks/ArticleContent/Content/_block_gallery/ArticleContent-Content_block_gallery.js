import React from 'react';
import {declMod} from 'bem-react-core';
import Image from 'e:Image m:size_wide'

export default declMod({block: 'gallery'}, {
    block: 'articlecontent',
    elem: 'content',
   
    content({images}) {
        return images.map((el, index) => (
            <Image key={index} size={el.size} image={el.image} caption={el.caption} thumbnail={el.thumbnail} /> 
        ));
    }
});
