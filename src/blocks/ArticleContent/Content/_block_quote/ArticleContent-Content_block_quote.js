import React, {Fragment} from 'react';
import {declMod} from 'bem-react-core';
import Icon from 'b:Icon';
import {ICONS} from '../../../Icon/icon_constants';

export default declMod({block: 'quote'}, {
    block: 'articlecontent',
    elem: 'content',
    content({content}) {
        return (
            <Fragment>
                <div className="articlecontent-content-wrap">
                    <div className="articlecontent-content-placeholder">
                        <Icon size={30} icon={ICONS.QUOTE} color={"#1875F0"}/>
                    </div>
                    <div className="articlecontent-content-text" dangerouslySetInnerHTML={{__html: content.content}}>
                    </div>
                        <div className="articlecontent-content-author" dangerouslySetInnerHTML={{__html: content.name}}>
                    </div>
                </div>
            </Fragment>
        );
    }
});
