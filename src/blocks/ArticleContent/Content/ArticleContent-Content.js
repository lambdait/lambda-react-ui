import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';

export default decl({
    block: 'articlecontent',
    elem: 'content',
    mods({block}) {
        return {block};
    },     
    content({children}) {
        return (
            <Fragment>
                {children}
            </Fragment>
        )
    }
})
