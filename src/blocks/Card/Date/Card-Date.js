import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';

export default decl({
    block: 'card',
    elem: 'date',
    content({date_create}) {
        return(
            <Fragment>
               <span>{date_create}</span>
            </Fragment>
        )
    }
})
