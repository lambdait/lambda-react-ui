import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Title from 'e:Title';
import Thumbnail from 'e:Thumbnail';
import Tag from 'e:Tag';
import Date from 'e:Date';
import Head from 'e:Head';
import Author from 'e:Author';
import Views from 'e:Views';
import Comments from 'e:Comments';
import Footer from 'e:Footer';
import Body from 'e:Body';
import Descriptions from 'e:Descriptions';

export default decl({
    block: 'card',
    tag: 'a',
    attrs() {
        let res = {
            href: "#",
            ref: 'noreferrer'
        };
        return res
    },
    mods({size, image}) {
        return {size, image};
    },
    content({title, thumbnail, tag_color, tag_name, date_create, author, views, comment, descriptions}) {
        return (
            // TODO: Нормальный импорт икнок
            // TODO: Параметр время чтения
            <Fragment>
                <Head>
                    <Tag color={tag_color} tag_name={tag_name}/>
                    <Thumbnail url={thumbnail} title={title}/>
                </Head>
                <Body>
                <Title>{title}</Title>
                <Descriptions>{descriptions}</Descriptions>
                </Body>
                <Footer>
                    <Date date_create={date_create}/>
                    <Views>{views}</Views>
                    <Comments>{comment}</Comments>
                    <Author>{author}</Author>
                </Footer>
            </Fragment>
        )
    }
})
