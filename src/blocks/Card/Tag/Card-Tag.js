import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';


export default decl({
    block: 'card',
    elem: 'tag',
    tag: 'span',
    style({color}) {
        return {
            background: color,
        }
    },
    content({tag_name}) {
        return (
            <Fragment>
                {tag_name}
            </Fragment>
        )
    }
})
