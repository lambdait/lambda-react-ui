import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';

export default decl({
    block: 'card',
    elem: 'body',
    content({children}) {
        return(
            <Fragment>
                {children}
            </Fragment>
        )
    }
})
