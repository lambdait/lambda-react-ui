import React, {Fragment} from 'react';
import {decl} from 'bem-react-core';
import Icon from 'b:Icon';
import {ICONS} from '../../Icon/icon_constants';

export default decl({
    block: 'card',
    elem: 'views',
    content({children}) {
        return (
            <Fragment>
                <Icon size={30} icon={ICONS.EYE} color={"#d8d8d8"}/>
                <span>{children}</span>
            </Fragment>
        )
    }
})
