import React, {Fragment} from 'react';
import {declMod} from "bem-react-core";
import {ICONS} from "../../Icon/icon_constants";
import Title from 'e:Title';
import Thumbnail from 'e:Thumbnail';
import Tag from 'e:Tag';
import Date from 'e:Date';
import Head from 'e:Head';
import Author from 'e:Author';
import Views from 'e:Views';
import Comments from 'e:Comments';
import Footer from 'e:Footer';
import Body from 'e:Body';
import Descriptions from 'e:Descriptions';

export default declMod({size: 'xxl'}, {
    block: 'card',
    content({title, thumbnail, tag_color, tag_name, date_create, author, views, comment, descriptions}) {
        return (
            <Fragment>
                <Body>
                    <Thumbnail url={thumbnail} title={title}/>
                <Head>
                    <Tag color={tag_color} tag_name={tag_name}/>
                    <Title>{title}</Title>
                    <Descriptions>{descriptions}</Descriptions>
                    <Footer>
                        <Date date_create={date_create}/>
                        <Views>{views}</Views>
                        <Comments>{comment}</Comments>
                        <Author>{author}</Author>
                    </Footer>
                </Head>


                </Body>

            </Fragment>
        );
    }
});
