import React from 'react';
import {decl} from 'bem-react-core';

export default decl({
    block: 'card',
    elem: 'title',
    attrs({link}) {
        return {
            href: link,
            rel: 'noreferrer'
        }
    },
    content({children}) {
        return (
            <span>
                {children}
            </span>
        )
    }
})
